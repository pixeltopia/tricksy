# tricksy!

This is a little graphics demo in JS using canvas.

## breakdown

- `index.html` contains the canvas and wires up the `demo` to the `main` rendering loop and api
- `layout.css` lays out the page
- `modules/main.js` contains the rendering loop and api
- `modules/demo.js` contains the logic for using the api

## testing locally

This project uses ES modules so you'll need to host these locally to get around CORS if you want to view the web page in a desktop browser. If you've got ruby installed, you can do this one-liner:

```
ruby -run -e httpd . -p 8080
```

Or run this, if you're on a Mac:

```
./run
```

Which will do the same thing.

