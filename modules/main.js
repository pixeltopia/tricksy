export default async ({ id, size, init, tick, draw, step = 1000 / 60 }) => {

    const surface = ({ id, size, smoothing = false }) => {
        const canvas = id ? document.getElementById(id) : document.createElement('canvas');
        const { width, height } = size || canvas.getBoundingClientRect();

        const context = canvas.getContext('2d');
        const ratio   = (window.devicePixelRatio || 1) / (context.backingStorePixelRatio || 1);
        canvas.width  = ratio * width;
        canvas.height = ratio * height;

        context.setTransform(ratio, 0, 0, ratio, 0, 0);
        context.imageSmoothingEnabled = smoothing;

        const stamp = ({ canvas }) => {
            context.drawImage(canvas, 0, 0, width, height);
        };

        return { canvas, context, size, stamp };
    };

    const image = async (url) =>
        new Promise((resolve, reject) => {
            const image   = new Image();
            image.onload  = () => resolve(image);
            image.onerror = reject;
            image.src     = url;
        });

    const rand = (lower, upper) =>
        lower + Math.round(Math.random() * (upper - lower));

    const screen = surface({ id });
    const buffer = surface({ size });

    const { context } = buffer;

    const stroke = ([ r, g, b ]) => {
        context.strokeStyle = `rgb(${r}, ${g}, ${b})`;
    };

    const fill = ([ r, g, b ]) => {
        context.fillStyle = `rgb(${r}, ${g}, ${b})`;
    };

    const line = (x0, y0, x1, y1) => {
        context.beginPath();
        context.moveTo(x0, y0);
        context.lineTo(x1, y1);
        context.stroke();
    };

    const rect = (x, y, width, height) => {
        context.fillRect(x, y, width, height);
    };

    const poly = (...points) => {
        const [ [ x0, y0 ], ...rest ] = points;
        context.beginPath();
        context.moveTo(x0, y0);
        for (const [ x, y ] of rest) {
            context.lineTo(x, y);
        }
        context.closePath();
        context.fill();
    };

    const circle = (x, y, r) => {
        context.beginPath();
        context.arc(x, y, r, 0, 2 * Math.PI);
        context.fill();
    };

    const graphics = {
        image,
        surface,
        fill,
        stroke,
        line,
        rect,
        poly,
        circle,
    };

    const vec = (x = 0, y = 0, z = 0, w = 1) => {
        return [ x, y, z, w ];
    };

    const add = (
        [ ax, ay, az ],
        [ bx, by, bz ],
    ) => {
        return vec(
            ax + bx,
            ay + by,
            az + bz,
        );
    };

    const subtract = (
        [ ax, ay, az ],
        [ bx, by, bz ],
    ) => {
        return vec(
            ax - bx,
            ay - by,
            az - bz,
        );
    };

    const dot = (
        [ ax, ay, az ],
        [ bx, by, bz ],
    ) => {
        return (ax * bx) + (ay * by) + (az * bz);
    };

    const cross = (
        [ ax, ay, az ],
        [ bx, by, bz ],
    ) => {
        return vec(
            (ay * bz) - (az * by),
            (az * bx) - (ax * bz),
            (ax * by) - (ay * bx),
        );
    };

    const magnitude = ([ x, y, z ]) =>
        Math.sqrt((x * x) + (y * y) + (z * z));

    const unit = ([ x, y, z ]) => {
        const factor = 1 / magnitude([ x, y, z ]);
        return vec(
            x * factor,
            y * factor,
            z * factor
        );
    };

    const average = (
        [ ax, ay, az ],
        [ bx, by, bz ],
        [ cx, cy, cz ],
    ) => {
        return vec(
            (ax + bx + cx) / 3,
            (ay + by + cy) / 3,
            (az + bz + cz) / 3,
        );
    };

    const matmulvec = (m, v) => {
        // see http://matrixmultiplication.xyz
        const [
            [ m00, m01, m02, m03 ],
            [ m10, m11, m12, m13 ],
            [ m20, m21, m22, m23 ],
            [ m30, m31, m32, m33 ],
        ] = m;
        const [
            x, y, z, w
        ] = v;
        return [
            (m00 * x) + (m01 * y) + (m02 * z) + (m03 * w),
            (m10 * x) + (m11 * y) + (m12 * z) + (m13 * w),
            (m20 * x) + (m21 * y) + (m22 * z) + (m23 * w),
            (m30 * x) + (m31 * y) + (m32 * z) + (m33 * w),
        ];
    };

    const matmulmat = (a, b) => {
        // see https://www.euclideanspace.com/maths/geometry/affine/matrix4x4/index.htm
        const [
            [ a00, a01, a02, a03 ],
            [ a10, a11, a12, a13 ],
            [ a20, a21, a22, a23 ],
            [ a30, a31, a32, a33 ],
        ] = a;
        const [
            [ b00, b01, b02, b03 ],
            [ b10, b11, b12, b13 ],
            [ b20, b21, b22, b23 ],
            [ b30, b31, b32, b33 ],
        ] = b;
        return [
            [
                (a00 * b00) + (a01 * b10) + (a02 * b20) + (a03 * b30),
                (a00 * b01) + (a01 * b11) + (a02 * b21) + (a03 * b31),
                (a00 * b02) + (a01 * b12) + (a02 * b22) + (a03 * b32),
                (a00 * b03) + (a01 * b13) + (a02 * b23) + (a03 * b33),
            ],
            [
                (a10 * b00) + (a11 * b10) + (a12 * b20) + (a13 * b30),
                (a10 * b01) + (a11 * b11) + (a12 * b21) + (a13 * b31),
                (a10 * b02) + (a11 * b12) + (a12 * b22) + (a13 * b32),
                (a10 * b03) + (a11 * b13) + (a12 * b23) + (a13 * b33),
            ],
            [
                (a20 * b00) + (a21 * b10) + (a22 * b20) + (a23 * b30),
                (a20 * b01) + (a21 * b11) + (a22 * b21) + (a23 * b31),
                (a20 * b02) + (a21 * b12) + (a22 * b22) + (a23 * b32),
                (a20 * b03) + (a21 * b13) + (a22 * b23) + (a23 * b33),
            ],
            [
                (a30 * b00) + (a31 * b10) + (a32 * b20) + (a33 * b30),
                (a30 * b01) + (a31 * b11) + (a32 * b21) + (a33 * b31),
                (a30 * b02) + (a31 * b12) + (a32 * b22) + (a33 * b32),
                (a30 * b03) + (a31 * b13) + (a32 * b23) + (a33 * b33),
            ],
        ];
    };

    const identity =
        [
            [ 1, 0, 0, 0 ],
            [ 0, 1, 0, 0 ],
            [ 0, 0, 1, 0 ],
            [ 0, 0, 0, 1 ],
        ];

    const scale = (sx, sy, sz) =>
        [
            [ sx,  0,  0, 0 ],
            [  0, sy,  0, 0 ],
            [  0,  0, sz, 0 ],
            [  0,  0,  0, 1 ],
        ];

    const translate = (dx, dy, dz) =>
        [
            [ 1, 0, 0, dx ],
            [ 0, 1, 0, dy ],
            [ 0, 0, 1, dz ],
            [ 0, 0, 0,  1 ],
        ];

    const rotateX = (radians) => {
        const cos = Math.cos(radians);
        const sin = Math.sin(radians);
        return [
            [ 1,    0,   0, 0 ],
            [ 0,  cos, sin, 0 ],
            [ 0, -sin, cos, 0 ],
            [ 0,    0,   0, 1 ],
        ];
    };

    const rotateY = (radians) => {
        const cos = Math.cos(radians);
        const sin = Math.sin(radians);
        return [
            [  cos, 0, -sin, 0 ],
            [    0, 1,    0, 0 ],
            [  sin, 0,  cos, 0 ],
            [    0, 0,    0, 1 ],
        ];
    };

    const rotateZ = (radians) => {
        const cos = Math.cos(radians);
        const sin = Math.sin(radians);
        return [
            [  cos, sin, 0, 0 ],
            [ -sin, cos, 0, 0 ],
            [    0,   0, 1, 0 ],
            [    0,   0, 0, 1 ],
        ];
    };

    const rotate = (rx, ry, rz) =>
        matmulmat(matmulmat(rotateX(rx), rotateY(ry)), rotateZ(rz));

    const project = (vecs, [ midx, midy ], factor) =>
        vecs.map(([ x, y, z, w ]) => {
            const scaling = factor / (factor + z);
            const scaled  = scale(scaling, scaling, 1);         // scale it using z
            const invert  = matmulmat(scale(1, -1, 1), scaled); // invert on y axis
            const screen  = matmulvec(invert, [ x, y, z, w ]);  // convert to screen space
            const offset  = translate(midx, midy, 0);           // offset to screen middle
            const result  = matmulvec(offset, screen);
            return result;
        });

    const deg2rad = (deg) => deg * Math.PI / 180;
    const rad2deg = (rad) => rad * 180 / Math.PI;

    const geometry = {
        vec,
        add,
        subtract,
        dot,
        cross,
        magnitude,
        unit,
        average,
        matmulvec,
        matmulmat,
        identity,
        scale,
        translate,
        rotateX,
        rotateY,
        rotateZ,
        rotate,
        project,
        deg2rad,
        rad2deg,
    };

    const ticker = (interval) => {
        let value = 0;
        return () => {
            if ((value = value + step) < interval) {
                return false;
            }
            value = 0;
            return true;
        };
    };

    await init({ ticker, geometry, graphics });

    let last        = undefined;
    let accumulated = 0;
    let delta       = 0;
    let ticked      = false;

    const frame = (now) => {
        if (last === undefined) {
            last = now;
        }

        delta = now - last;
        last  = now;

        accumulated += delta;

        ticked = false;

        // fixed timestep ticking
        while (accumulated >= step) {
            tick({ size, step, rand, geometry });
            accumulated -= step;
            ticked = true;
        }

        if (ticked) {
            draw({ screen, buffer, graphics, geometry });
            screen.stamp(buffer);
        }

        window.requestAnimationFrame(frame);
    };

    window.requestAnimationFrame(frame);
};
