export default (id, main) => {

    const size = { width: 200, height: 200 };

    const PALETTE = [
        [ 0xff, 0x66, 0x98 ],
        [ 0xff, 0xb3, 0x66 ],
        [ 0xff, 0xff, 0x66 ],
        [ 0x98, 0xff, 0x66 ],
        [ 0x66, 0x98, 0xff ],
        [ 0x88, 0x00, 0xff ],
        [ 0x00, 0x44, 0x88 ], // BACK
    ];

    const colour = (index) => PALETTE[index % PALETTE.length];

    let triangles;
    let middle;
    let points;
    let angle;
    let distance;
    let counter;
    let working;
    let ticked;

    let normal;

    const init = async ({
        ticker,
        geometry: { vec },
    }) => {
        ticked = ticker(50);

        angle    = 0;
        distance = 0;
        counter  = 0;

        middle = vec(size.width / 2, size.height / 2);
        points = [
            vec(-1,  1, -1), // FTL
            vec( 1,  1, -1), // FTR
            vec( 1, -1, -1), // FBR
            vec(-1, -1, -1), // FBL
            vec(-1,  1,  1), // BTL
            vec( 1,  1,  1), // BTR
            vec( 1, -1,  1), // BBR
            vec(-1, -1,  1), // BBL
        ];

        const FTL = 0;
        const FTR = 1;
        const FBR = 2;
        const FBL = 3;
        const BTL = 4;
        const BTR = 5;
        const BBR = 6;
        const BBL = 7;

        triangles = [
            [FBL, FTL, FBR, 0], // FRONT
            [FTR, FBR, FTL, 0],

            [FBR, FTR, BBR, 1], // RIGHT
            [BTR, BBR, FTR, 1],

            [BTL, BBL, BTR, 2], // BACK
            [BBR, BTR, BBL, 2],

            [BBL, BTL, FBL, 3], // LEFT
            [FTL, FBL, BTL, 3],

            [FTL, BTL, FTR, 4], // TOP
            [BTR, FTR, BTL, 4],

            [BBL, FBL, BBR, 5], // BOTTOM
            [FBR, BBR, FBL, 5],
        ];
    };

    const tick = ({
        geometry: {
            matmulmat,
            matmulvec,
            scale,
            translate,
            rotate,
            deg2rad,
        }
    }) => {
        if (! ticked() && working) {
            return;
        }

        angle   += deg2rad(4);
        counter += deg2rad(5);
        distance = 100 + Math.sin(counter) * 100;

        const transform = matmulmat(
            matmulmat(
                translate(0, 0, distance),  // move it back and forth
                scale(50, 50, 50),          // scale it up
            ),
            rotate(angle, 0, angle / 2)     // rotate it
        );

        working = points.map(it => matmulvec(transform, it));
    };

    const draw = ({
        buffer: {
            size: { width, height }, canvas, context, stamp
        },
        graphics: {
            surface, fill, stroke, line, rect, circle, poly
        },
        geometry: {
            project, deg2rad, vec, add, subtract, dot, cross, unit, scale, matmulvec, average
        }
    }) => {
        fill(colour(6));
        rect(0, 0, width, height);

        const lookup = ([ i0, i1, i2 ]) =>
            [ working[i0], working[i1], working[i2] ];

        const facing = (vector, [ p0, p1, p2 ]) =>
            dot(unit(cross(subtract(p1, p0), subtract(p2, p0))), vector);

        const lerp = (a, b, t) =>
            (1 - t) * a + t * b;

        const mixed = ([ r0, g0, b0 ], [ r1, g1, b1 ], amount) =>
            [ lerp(r0, r1, amount), lerp(g0, g1, amount), lerp(b0, b1, amount) ];

        const camera = vec(0, 0, 1);
        const light  = unit(vec(0, -1, 1));

        for (const triangle of triangles) {
            const points = lookup(triangle);

            if (facing(camera, points) >= -0.25) {
                continue;
            }

            // facing = -1.0 to 1.0
            // / 2    = -0.5 to 0.5
            // + 0.5  =  0.0 to 1.0
            const amount = facing(light, points) / 2 + 0.5;
            const [,,,c] = triangle;

            fill(mixed(colour(c), [ 0x00, 0x11, 0x22 ], amount));
            poly(...project(points, middle, 200));
        }

        warp({ surface, canvas, context, stamp, deg2rad });
    };

    let   offscreen;
    let   warpAngle          = 0;
    const warpAmountAngle    = 0;
    const warpAngleIncrement = 2;
    const warps              = 15;
    const startAlpha         = 0.1;
    const warpScale          = 1.1;

    const warp = ({ surface, canvas, context, stamp, deg2rad }) => {
        if (! offscreen) {
            offscreen = surface({ size });
        }

        const [ midx, midy ] = middle;

        offscreen.stamp({ canvas });

        context.save();
        context.globalAlpha = startAlpha;
        context.globalCompositeOperation = 'lighten';

        for (let next = warps; next; next--) {
            context.translate(midx, midy);
            context.rotate(Math.sin(warpAngle) * deg2rad(warpAmountAngle));
            context.scale(warpScale, warpScale);
            context.translate(-midx, -midy);
            stamp(offscreen);
            context.globalAlpha -= startAlpha / warps;
        }

        context.restore();

        warpAngle += deg2rad(warpAngleIncrement);
    };

    main({ id, size, init, tick, draw });

};
